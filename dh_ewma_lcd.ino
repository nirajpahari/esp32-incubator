#include "DHT.h"
#include <LiquidCrystal_I2C.h>

//DHT connections
//+ to GND
//- to vcc
// out to 4

//DHT definations
#define DHTPIN 4
#define DHTTYPE DHT22

//Definations for relay
#define RELAY1  7
#define RELAY2  8
#define RELAY3  9

DHT dht(DHTPIN, DHTTYPE);

//LCD definations
int lcdColumns=16;
int lcdRows = 2;

//Definations for 'Exponentially Weighted Moving Average' (EWMA)
float EWMA_a = 0.1;
float EWMA_s_t = 0.0, EWMA_s_h = 0.0 ;



void lcd_th_show( float t, float h);

LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println(F("System Started"));
  lcd.init();
  lcd.backlight();
  lcd.clear();

  dht.begin();
  EWMA_s_t = dht.readTemperature();
  EWMA_s_h = dht.readHumidity();

  // Pinmode definations for relay
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);
  pinMode(RELAY3, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  //wait for 2 seconds between meseurements
  delay(2000);
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  // check the readings and exit if reading is empty
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read. Trying again"));
    return;
  }
  else
  {
//    Serial.println(t);
//    Serial.println(h);
    EWMA_s_t = ( EWMA_a * t) + (( 1-EWMA_a)*EWMA_s_t);
    EWMA_s_h = ( EWMA_a * h) + (( 1-EWMA_a)*EWMA_s_h);
//    Serial.println(EWMA_s_t);
//    Serial.println(EWMA_s_h);
    }
  lcd_th_show(EWMA_s_t, EWMA_s_h);
  digitalWrite(RELAY1,HIGH);
  digitalWrite(RELAY2,LOW);
  digitalWrite(RELAY3,LOW);
  
// Check sensor readings for actions
  if (EWMA_s_t > 26.0){
    // Turn on the fan
    
    }
  else if (EWMA_s_t < 21.0){
    // Turn on the hear source may be light and turn off the fan
    
    }

  if (EWMA_s_h < 60) {
    // Turn on the humidifier
  }
  else if (EWMA_s_h >70) {
    // Turn off the humidifier
  }
  
}

void lcd_th_show( float t, float h){

  lcd.setCursor(0,0);
  lcd.print("Humidity:");
  lcd.setCursor(10,0);
  lcd.print(EWMA_s_h);
  lcd.setCursor(0,1);
  lcd.print("Temp    :");
  lcd.setCursor(10,1);
  lcd.print(EWMA_s_t);
  
  }
